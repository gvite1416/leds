import io from 'socket.io-client';
const socket = io.connect('http://localhost:5000');
socket.on('connect', function(data) {
    console.log("socket io connected");
    socket.emit('is_open_port', {});
});
socket.on('arduino', response => {
    console.log('socket io ', response);
});
socket.on('send_button_status', response => {
    changeColorBtn(response.id, !response.status);
});
socket.on('is_open_port', response => {
    console.log(response);
});
window.changeColorBtn = (id, status) => {
    const btns = document.getElementsByClassName('btn-duino');
    for( const index in btns) {
        if(typeof btns[index] == 'object') {
            if(btns[index].getAttribute('data-id') == id) {
                btns[index].setAttribute('data-status' , status);
                if (status) {
                    btns[index].className = btns[index].className.replace(/\bactive\b/g, "disable");
                } else {
                    btns[index].className = btns[index].className.replace(/\bdisable\b/g, "active");
                }
            }
        }
    }
}
window.setButton = (elmnt) => {
    const status = (elmnt.getAttribute('data-status') == 'true') ? true : false;
    const id = parseInt(elmnt.getAttribute('data-id'));

    socket.emit('send_button', {id: id, status: status});
}