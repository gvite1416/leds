'use strict';

var gulp = require('gulp')
  , nodemon = require('gulp-nodemon');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var log = require('fancy-log');
const webpack = require('webpack');
const gulpWebpack = require('webpack-stream');
sass.compiler = require('node-sass');

gulp.task('sass',  () => {
    log('sass');
    return gulp.src('./assets/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/css'));
});
gulp.task('js', () => {
    return gulp.src('./assets/js/app.js')
        .pipe(gulpWebpack({
            output: {
                filename: 'app.js',
            }
        }, webpack))
        .pipe(gulp.dest('public/js'));
});
gulp.task('sass:watch',  () => {
    gulp.watch('./assets/sass/**/*.scss', gulp.parallel('sass'));
});
gulp.task('js:watch',  () => {
    gulp.watch('./assets/js/**/*.js', gulp.parallel('js'));
});
gulp.task('start',  (done) => {
    nodemon({
        script: 'app.js',
        ignore: ['assets/'],
        ext: 'js html',
        env: { 'NODE_ENV': 'development' },
        done: done
    });
});
gulp.task('icons', () => {
    return gulp.src('./node_modules/@fontawesome/fontawesome-free/webfonts/**.*')
        .pipe(gulp.dest('./public/fonts/'));
});

gulp.task('default', gulp.parallel('start', 'sass:watch', 'js:watch'));