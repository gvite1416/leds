/**
 * Created by @gvite on 10/02/17.
 */

// Express web framework, more information: http://expressjs.com/
// Instance of express
const express = require('express');
const serialport = require('serialport');
const Twitter = require('twitter');

const env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

//Environment setup
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
// get our config file
const config = require('./config/config')[env];
const clientTw = new Twitter({
    consumer_key: config.tw.consumer_key,
    consumer_secret: config.tw.consumer_secret,
    access_token_key: config.tw.access_token_key,
    access_token_secret: config.tw.access_token_secret
});
require('./config/express')(app, config, clientTw);
require('./config/socket.io')(app, serialport, io, clientTw);


// require('./config/mongoose')(config);

// Environment setup
http.listen(config.port, () => {
    console.log('Gulp is running my app on PORT: ' + config.port)
})