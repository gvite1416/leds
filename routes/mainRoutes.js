/**
 * Created by @gvite on 1/6/16.
 */
var express = require('express');

var routes = function(clientTw) {
    var mainRouter = express.Router();
    var mainController = require('../controllers/mainController')(clientTw);
    mainRouter.route('/')
        .get(mainController.get);
    return mainRouter;
};

module.exports = routes;