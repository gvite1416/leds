/**
 * Created by @gvite on 10/02/17.
 */
//var Helpers = require('../../helpers/v1/stringHelper');

const ioController = (port, clientTw) => {
    let client;
    const onConnected = (data) => {
        console.log('IO data', data);
    };
    const onSendButton = (data) => {
        console.log(data);
        const status = data.status ? ',1' : ',0';
        const write = data.id + status;
        port.write(write);
        client.emit('send_button_status', {success: true, id: data.id, status: data.status, write: write});
    }
    const setClient = (sclient) => {
        client = sclient;
    }
    const isOpenPort = () => {
        client.emit('is_open_port', {success: true, isOpen: port.isOpen});
    }
    return {
        onConnected: onConnected,
        onSendButton: onSendButton,
        setClient: setClient,
        isOpenPort: isOpenPort
    }
};

module.exports = ioController;