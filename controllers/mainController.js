/**
 * Created by @gvite on 10/02/17.
 */
//var Helpers = require('../../helpers/v1/stringHelper');

const mainController = (clientTw) => {
    var params = {screen_name: 'Vite666'};
    const get = (req, res) => {
        console.log('GET main');
        clientTw.get('statuses/user_timeline', params, function(error, tweets, response) {
            if (!error) {
                console.log(tweets);
            }
            res.status(200).render('index' , {title: 'LEDS'});
        });
    };
    return {
        get: get
    }
};

module.exports = mainController;