/**
 * Created by @gvite on 10/02/17.
 */
//var Helpers = require('../../helpers/v1/stringHelper');

const serialPortController = (clientTw) => {
    let client;
    const onOpen = () => {
        console.log("Arduino conectado");
        if(client)
            client.emit('arduino', {success: true, message: 'Arduino Conectado'});
    };
    const onData = (data) => {
        console.log('Data: ' + data);
        client.emit('arduino_data', {success: true, data: data});
    };
    const setClient = (sclient) => {
        client = sclient;
    }
    return {
        onOpen: onOpen,
        onData: onData,
        setClient: setClient
    }
};

module.exports = serialPortController;