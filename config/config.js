/**
 * Created by @gvite on 10/02/17.
 */
var path = require('path');
var rootPath = path.normalize(__dirname + '/../');

module.exports = {
    development: {
        rootPath: rootPath,
        pathResources: "http://localhost:5000",
        port: process.env.PORT || 5000,
        secret: 'TiASddmSpttNA.15',
        // 'database': 'mongodb://localhost/carnal-epitaph',
        rateLimits: {
            ttl: 10 * 60 * 1000, //10 minutes
            maxHits: 1200 // Max Hits
        },
        tw: {
            consumer_key: 'I4JK1g2QZdrvHKbpQBdbkHmoR',
            consumer_secret: 'oA7vJW4xHxTChvJKkRcQ5SVftNWbOIm2MyNEL454RM9DvK20pC',
            access_token_key: '127925045-yp9W5ddyK5ZfHRBAbuoIzD3BJfzZOfcKW2TaB799',
            access_token_secret: 'WDn76335KaU0ujTT4tJkn2qE8mPyu6jJv4zJfcgQewiPc'
        }
    },
    production: {
        rootPath: rootPath,
        pathResources: "https://infinite-fortress-79762.herokuapp.com/",
        port: process.env.PORT || 5001,
        'secret': 'TiASddmSpttNA.15',
        // 'database': 'mongodb://heroku_cs8g2mft:9722h0ig47v7896lmo4o2dsujf@ds161913.mlab.com:61913/heroku_cs8g2mft',
        rateLimits: {
            ttl: 10 * 60 * 1000, //10 minutes
            maxHits: 1200 // Max Hits
        }
    }
};