/**
 * Created by @gvite on 10/02/17.
 */
const express = require('express'),
    bodyParser = require('body-parser');
    moment = require('moment');

// ## CORS middleware
// see: http://stackoverflow.com/questions/7067966/how-to-allow-cors-in-express-nodejs
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
    //res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
        res.send(200);
    }else {
        next();
    }
};

module.exports = (app, config, clientTw) => {
    //Using body-parser more information: https://github.com/expressjs/body-parser
    app.set("view engine" , "pug");
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    // configure static folder
    app.use(express.static(config.rootPath + 'public'));
    app.use(allowCrossDomain);
    app.locals.moment = moment;
    // Routes, using injection to pass the model
    mainRouter = require('../routes/mainRoutes')(clientTw);
    app.use('/', mainRouter);
};