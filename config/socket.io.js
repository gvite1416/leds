/**
 * Created by @gvite on 10/02/17.
 */

module.exports = (app, SerialPort, io, clientTw) => {
    const Readline = SerialPort.parsers.Readline;
    const parser = new Readline();
    const port = new SerialPort('/dev/cu.usbmodem1421');
    port.pipe(parser);
    const serialPortController = require('../controllers/serialPortController')(clientTw);
    port.on('open', serialPortController.onOpen);
    port.on('data', serialPortController.onData);
    // const hashs = 'ArduinoLedAmarillo,ArduinoLedAzul,ArduinoLedVerde,ArduinoLedRojo,Enciende,Apaga';
    const hashs = 'ADS';
    const ioController = require('../controllers/io/ioController')(port, clientTw);
    var stream = clientTw.stream('statuses/filter', {track: hashs});
    io.on('connection', client => {
        console.log("client connected");
        serialPortController.setClient(client);
        ioController.setClient(client);
        client.on('connected', ioController.onConnected);
        client.on('is_open_port', ioController.isOpenPort);
        client.on('send_button', ioController.onSendButton);
        stream.on('data', (event) => {
            if(event && event.entities.hashtags.length > 0){
                console.log(event.text);
                const hashsArray = hashs.split(',');
                let status;
                let led;
                event.entities.hashtags.forEach(hash => {
                    let position = hashsArray.indexOf(hash.text);
                    if (position !== -1){
                        port.write(event.text);        
                    }
                });
                // client.emit('send_button_status', {success: true, id: led, status: status, write: led + ',' +  status});
            }
            
        });
    });
};