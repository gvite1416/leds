/**
 * Created by @gvite on 10/02/17.
 */
var mongoose = require('mongoose'),
    userModel = require('../models/user');

module.exports = function (config) {
    mongoose.connect(config.database);
    var db = mongoose.connection;

    db.on('error', console.error.bind(console, 'connection error...'));
    db.once('open', function callback() {
        console.log('API Carnal Epitaph');
    });

    userModel.createDefaultUsers;
};